using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public Material[] mat1;

    public Material[] mat2;

    public Material[] mat3;

    float currentTime = 0f;

    float timer1 = 5f;

    float timer2 = 3f;

    float timer3 = 10f;

    public GameObject wallCar;

    public GameObject wallPlayer;

    bool isPressed = false;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = timer1;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPressed)
        {
            TrafficSystem();
        }
    }

    void TrafficSystem()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        currentTime -= Time.deltaTime;
        if (currentTime < 1f)
        {
            meshRenderer.materials = mat3;
            timer1 -= Time.deltaTime;
            wallCar.SetActive(true);
            wallPlayer.SetActive(false);
            if (timer1 < 1f)
            {
                meshRenderer.materials = mat1;
                timer2 -= Time.deltaTime;
                if (timer2 < 1)
                {
                    meshRenderer.materials = mat2;
                    wallCar.SetActive(false);
                    wallPlayer.SetActive(true);
                    timer1 = 5f;
                    timer2 = 3f;
                    currentTime = timer1;
                }
            }
        }
    }

    public void OnButtonPressed()
    {
        isPressed = true;
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        
        if(isPressed)
        {
            timer2 -= Time.deltaTime;
        
            if(timer2 <= 0)
            {
                meshRenderer.materials = mat3;
                wallCar.SetActive(true);
                wallPlayer.SetActive(false);
                timer2 = 3f;
                timer1 -= Time.deltaTime;
                if(timer1 <= 0)
                {
                    isPressed = false;
                    timer1 = 10f;
                }
            }
        }
    }
}
