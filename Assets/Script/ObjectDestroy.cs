using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectDestroy : MonoBehaviour
{
    public float lifeTime = 20f;

    void Update()
    {
        if(lifeTime > 0)
        {
            lifeTime -= Time.deltaTime;
            if(lifeTime <= 0)
            {
                Destruction();
            }
        }
        if(this.transform.position.x <= -35)
        {
            Destruction();
            
        }
        if(this.transform.position.x > 19)
        {
            Destruction();
        } 
          
    }

    void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.name == "destroyer")
        {
            Destruction();
        }
    }

    void Destruction()
    {
        Destroy(this.gameObject);
    }
}
