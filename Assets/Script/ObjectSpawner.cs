using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] objectToSpawn;
    public GameObject bus;
    public Transform[] car;

    void Start()
    {
        InvokeRepeating("SpawnObject0", 5, 20);
        InvokeRepeating("SpawnObject1", 3, 5);
        InvokeRepeating("SpawnObject2", 2, 6);
        InvokeRepeating("SpawnObject3", 4, 5);
    }

     public void SpawnObject0()
    {
        // yield return new WaitForSeconds(5);
        Instantiate(bus, car[0].position, car[0].rotation);
    }

    public void SpawnObject1()
    {
        // yield return new WaitForSeconds(3);
        int randomIndex = Random.Range(0, objectToSpawn.Length);
        Instantiate(objectToSpawn[randomIndex], car[1].position, car[1].rotation);
    }

    public void SpawnObject2()
    {
        // yield return new WaitForSeconds(4);
        int randomIndex = Random.Range(0, objectToSpawn.Length);
        Instantiate(objectToSpawn[randomIndex], car[2].position, car[2].rotation);
    }

    public void SpawnObject3()
    {
        // yield return new WaitForSeconds(3);
        int randomIndex = Random.Range(0, objectToSpawn.Length);
        Instantiate(objectToSpawn[randomIndex], car[3].position, car[3].rotation);
    }
}
