// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using UnityEngine.SceneManagement;
// using UnityEngine.UI;

// public class Player : MonoBehaviour
// {
//     public float maxHealth = 100;
//     public float maxOxygen = 100;

//     public float currentHealth;
//     public float currentOxygen;

//     public float radius = 3;

//     public float damageAmount = 1;

//     public bool isGameOver = false;

//     // public HealthBar healthBar;
//     // public OxygenBar oxygenBar;
//     // public PhysicMaterial material;
//     public GameObject fireOverlay;


//     Collider coll;

//     void Start()
//     {
//         currentHealth = maxHealth;
//         // healthBar.SetMaxHealth (maxHealth);

//     }

//     void OnParticleCollision(GameObject other)
//     {
        
//             OxygenDamaged(0.1f);
        
//     }
    
//     private void OnControllerColliderHit(ControllerColliderHit hit)
//     {
//         // Rigidbody body = hit.collider.attachedRigidbody;
//         if (hit.collider.gameObject.tag == "Player")
//         {
//             coll = GetComponent<Collider>();
//             StartCoroutine(TakeDamage (0.5f));
//         }
//     }

//     void OxygenDamaged(float damage)
//     {
//         currentOxygen -= damage;
//         oxygenBar.SetOxygen(currentOxygen);
//         if(currentOxygen < 1)
//         {
//             StartCoroutine(TakeDamage (0.1f));
//         }
//     }
//     IEnumerator TakeDamage(float damage)
//     {
//         fireOverlay.SetActive(true);
//         currentHealth -= damage;

//         healthBar.SetHealth (currentHealth);
//         if (currentHealth < 1)
//         {
//             currentHealth = 0;
//             isGameOver = true;
//         }
//         yield return new WaitForSeconds(1f);
//         fireOverlay.SetActive(false);
//     }
// }
